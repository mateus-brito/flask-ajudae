$(".login").submit(function(e) {
    e.preventDefault();
    var form_data = new FormData($('.login')[0]);
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        async: false,
        success: function(data) {
            location.reload();
        },
        error: function(data) {
            $('.login > span').html(JSON.parse(data.responseText).message);
        }
    });
});
$(".register").submit(function(e) {
    e.preventDefault();
    var form_data = new FormData($('.register')[0]);
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        async: false,
        success: function(data) {
            location.reload();
        },
        error: function(data) {
            $('.register > span').html(JSON.parse(data.responseText).message);
        }
    });
});
$("#register_institution").submit(function(e) {
    e.preventDefault();
    var form_data = new FormData($('#register_institution')[0]);
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        async: false,
        success: function(data) {
            //location.reload();
        },
        error: function(data) {
            $('.register > span').html(JSON.parse(data.responseText).message);
        }
    });
});

function logout(){
    $.ajax({
        url: "/logout/",
        type: 'POST',
        contentType: false,
        cache: false,
        processData: false,
        async: false,
        success: function(data) {
            location.reload();
        },
        error: function(data) {
            //$('.register > span').html(JSON.parse(data.responseText).message);
        }
    });

}
