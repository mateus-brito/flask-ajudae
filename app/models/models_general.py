from sqlalchemy import Numeric,PrimaryKeyConstraint
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, Integer, ForeignKey, Text, DateTime, String, Boolean
from app.base.CustomBase import CustomBase

from app.database import db

class User(CustomBase):
    __tablename__ = "user"

    id = Column(Integer,primary_key=True,autoincrement=True)

    username = Column(Text)
    email = Column(Text)
    password = Column(Text)

class Marker(CustomBase):
    __tablename__ = "marker"

    id = Column(Integer,primary_key=True,autoincrement=True)

    name = Column(Text)
    token = Column(Text)
    address = Column(Text)
    num_address = Column(Text)
    city = Column(Text)
    state = Column(Text)
    country = Column(Text)
    phone = Column(Text)
    website = Column(Text)
    place_id = Column(Text)
    restored = Column(Boolean)
    lat = Column(Text)
    lng = Column(Text)
    active = Column(Integer)
    user_id = Column(Integer, ForeignKey('user.id'))
    observation = Column(Text)

    @property
    def serialize(self):
        user = User.query.filter_by(id=self.user_id).first()
        return {
           'id'         : self.id,
           'name'  : self.name,
           'address'  : self.address,
           'num_address'  : self.num_address,
           'city'  : self.city,
           'state'  : self.state,
           'country'  : self.country,
           'phone'  : self.phone,
           'website'  : self.website,
           'place_id'  : self.place_id,
           'lat'  : self.lat,
           'lng'  : self.lng,
           'user': user.username if user else 'bot'
       }
    
class Comment(CustomBase):

    id = Column(Integer,primary_key=True,autoincrement=True)
    foto_id = Column(Integer, ForeignKey('photo.id'))
    marker_token = Column(Integer, ForeignKey('marker.token'))
    rating = Column(Integer)
    description = Column(Text)

    @property
    def serialize(self):
       return {
           'id': self.id,
           'foto_id': self.foto_id,
           'marker_token': self.marker_token,
           'rating': self.rating,
           'description': self.description
       }
       
class Photo(CustomBase):

    id = Column(Integer,primary_key=True,autoincrement=True)
    foto_url = Column(String(250))

    @property
    def serialize(self):
       return {
           'foto_url': self.foto_url,
       }