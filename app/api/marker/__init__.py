from flask import (
    Blueprint,
    render_template,
    abort,
    redirect,
    url_for,
    request,
    jsonify,
    current_app,
)
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token,
    get_jwt_identity,
)
from app.models import *
from app.database import db
from app.models import Marker
from app.views import serialize, serialize_custom, save_optmize_image, allowed_file
import json
import sys
import requests
import uuid
import os

api_marker = Blueprint("api_marker", __name__, template_folder="templates")


def custom_comments(custom):

    photo = Photo.query.filter_by(id=custom.foto_id).first()

    return {"foto_url": photo.foto_url if photo else ""}


@api_marker.route("/comment/get", methods=["GET"])
def getComment():
    token_id = request.args.get("user_token")
    page = request.args.get("page", 1)

    comments = (
        db.session.query(Comment.id, Comment.foto_id, Comment.rating, Comment.description)
        .filter_by(marker_token=token_id)
        .paginate(page, 100, False)
    )

    return serialize_custom(comments.items, custom_comments)


@api_marker.route("/comment/public", methods=["POST"])
def makeComment():

    token = request.form.get("token")
    rating = request.form.get("rating")
    description = request.form.get("comment")
    file = request.files["file"] if "file" in request.files else None
    photo_id = None

    if file and allowed_file(file.filename):
        prefix = str(uuid.uuid4().hex)
        name_image = "fotos/" + save_optmize_image(
            file, path_prefix="", prefix=prefix + "_"
        )
        photo = Photo(foto_url=name_image)
        db.session.add(photo)
        db.session.flush()
        photo_id = photo.id

    comment = Comment(
        foto_id=photo_id, rating=rating, description=description, marker_token=token
    )
    db.session.add(comment)
    db.session.commit()

    return "ok", 200


@api_marker.route("/get/user/<id>", methods=["GET"])
def getUser(id):
    page = request.args.get("page", 1)

    res = (
        db.session.query(
            db.func.max(Marker.updated_on).label("updated_on"),
            Marker.id,
            Marker.token,
            Marker.name,
            Marker.address,
            Marker.num_address,
            Marker.city,
            Marker.state,
            Marker.country,
            Marker.phone,
            Marker.lat,
            Marker.lng,
            Marker.created_on,
        )
        .filter(Marker.user_id == id, Marker.lat != None)
        .group_by(Marker.token)
        .having(Marker.active == 1)
        .paginate(page, 100, False)
    )

    return serialize(res.items)


def custom_index(custom):
    author = User.query.filter_by(id=custom.user_id).first()
    username = author.username if author else "bot"

    comments = Comment.query.filter_by(marker_token=custom.token).all()
    mean_rating = [c.rating for c in comments]
    mean_rating = 0 if len(mean_rating) == 0 else sum(mean_rating) / len(mean_rating)

    return {
        "by_username": username,
        "rating": int(mean_rating),
        "total_rating": len(comments),
    }


@api_marker.route("/get", methods=["GET"])
def index():
    page = request.args.get("page", 1)
    token = request.args.get("token", "%")

    res = (
        db.session.query(
            db.func.max(Marker.updated_on).label("updated_on"),
            Marker.id,
            Marker.token,
            Marker.name,
            Marker.address,
            Marker.num_address,
            Marker.city,
            Marker.state,
            Marker.country,
            Marker.phone,
            Marker.lat,
            Marker.lng,
            Marker.user_id,
            Marker.created_on,
        )
        .filter(Marker.lat != None, Marker.token.ilike(token))
        .group_by(Marker.token)
        .having(Marker.active == 1)
        .all()
    )

    return serialize_custom(res, custom_index)


def custom_historic(historic):
    author = User.query.filter_by(id=historic.author).first()

    return {
        "author": author.username if author else "bot",
    }


@api_marker.route("/historic/<token_id>", methods=["GET"])
def getHistoric(token_id):
    page = request.args.get("page", 1)

    res = (
        db.session.query(
            Marker.id,
            Marker.name,
            Marker.address,
            Marker.num_address,
            Marker.city,
            Marker.state,
            Marker.country,
            Marker.phone,
            Marker.lat,
            Marker.lng,
            Marker.observation,
            Marker.user_id.label("author"),
            Marker.created_on,
            Marker.updated_on,
        )
        .filter_by(token=token_id)
        .order_by(Marker.updated_on.desc())
        .limit(100)
        .offset(1)
        .all()
    )
    return serialize_custom(res, custom_historic)


@api_marker.route("/create", methods=["POST"])
@jwt_required
def register_institution():

    current_user = get_jwt_identity()
    user = User.query.filter_by(email=current_user).first()

    name = request.json.get("name")
    address = request.json.get("address")
    num_address = request.json.get("num_address")
    city = request.json.get("city")
    state = request.json.get("state")
    country = request.json.get("country")
    website = request.json.get("website")
    phone = request.json.get("phone")
    observation = "Cadastro efetuado"

    lat, lng = getLatLng(num_address, address, city, state, country)

    m = Marker(
        token=str(uuid.uuid4().hex),
        user_id=user.id,
        name=name,
        address=address,
        num_address=num_address,
        active=1,
        city=city,
        restored=0,
        state=state,
        country=country,
        lat=lat,
        lng=lng,
        website=website,
        phone=phone,
        observation=observation,
    )

    db.session.add(m)
    db.session.commit()

    return jsonify({"status": True}), 200


@api_marker.route("/update", methods=["PUT"])
@jwt_required
def update_institution():

    current_user = get_jwt_identity()
    user = User.query.filter_by(email=current_user).first()

    token_id = request.json.get("token")
    name = request.json.get("name")
    address = request.json.get("address")
    num_address = request.json.get("num_address")
    city = request.json.get("city")
    state = request.json.get("state")
    country = request.json.get("country")
    website = request.json.get("website")
    phone = request.json.get("phone")
    observation = request.json.get("observation")

    lat, lng = getLatLng(num_address, address, city, state, country)

    marker_exist = Marker.query.filter_by(token=token_id).first_or_404()

    if lat == None or lng == None:
        lat = marker_exist.lat
        lng = marker_exist.lng

    m = Marker(
        token=marker_exist.token,
        user_id=user.id,
        name=name,
        address=address,
        num_address=num_address,
        active=1,
        restored=0,
        city=city,
        state=state,
        country=country,
        lat=lat,
        lng=lng,
        website=website,
        phone=phone,
        observation=observation,
    )
    db.session.add(m)
    db.session.commit()

    return jsonify({"status": True}), 200


@api_marker.route("/restore", methods=["PUT"])
@jwt_required
def restore():
    current_user = get_jwt_identity()
    user = User.query.filter_by(email=current_user).first()

    old_marker_id = request.json.get("id")

    old_marker = Marker.query.filter_by(id=old_marker_id).first_or_404()

    m = Marker(
        token=old_marker.token,
        user_id=user.id,
        name=old_marker.name,
        address=old_marker.address,
        num_address=old_marker.num_address,
        active=1,
        city=old_marker.city,
        state=old_marker.state,
        country=old_marker.country,
        lat=old_marker.lat,
        lng=old_marker.lng,
        restored=1,
        website=old_marker.website,
        phone=old_marker.phone,
        observation="Restaurando de #" + str(old_marker.id),
    )

    db.session.add(m)
    db.session.commit()

    return jsonify({"status": True}), 200


@api_marker.route("/delete", methods=["DELETE"])
@jwt_required
def delete():
    current_user = get_jwt_identity()
    user = User.query.filter_by(email=current_user).first()

    id = request.json.get("id")
    observation = request.json.get("observation")

    marker = Marker.query.filter_by(id=id).first_or_404()

    m = Marker(
        token=marker.token,
        user_id=user.id,
        name=marker.name,
        address=marker.address,
        num_address=marker.num_address,
        city=marker.city,
        state=marker.state,
        country=marker.country,
        lat=marker.lat,
        lng=marker.lng,
        website=marker.website,
        phone=marker.phone,
        active=0,
        observation=observation,
    )
    db.session.add(m)
    db.session.commit()

    return jsonify({"result": True}), 200


def getLatLng(num_address, address, city, state, country):
    key = current_app.config["GOOGLE_CREDENTIALS"]
    target_url = f"https://maps.googleapis.com/maps/api/geocode/json?address={num_address}+{city},{state},{country}&key={key}"
    r = requests.get(url=target_url)
    data = r.json()

    if len(data["results"]) > 0 and "geometry" in data["results"][0]:
        lat = data["results"][0]["geometry"]["location"]["lat"]
        lng = data["results"][0]["geometry"]["location"]["lng"]
        return [lat, lng]
    else:
        return [None, None]


@api_marker.route("/locale/id/<id>")
def data_by_id(id):
    result = Marker.query.filter_by(id=id).first()
    return result.serialize