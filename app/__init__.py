from flask import Flask
from app.views import default_view
from app.api.marker import api_marker
import os
import pathlib
from flask_migrate import Migrate
from flask_cors import CORS

from .JWTManager import jwt
from .database import db
from dynaconf import FlaskDynaconf
from app.models import *
from app.mail import mail


def create_app():

    basedir = os.path.abspath(os.path.dirname(__file__))
    app = Flask(__name__)
    app.url_map.strict_slashes = False
    app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
        basedir, "test.db"
    )

    CORS(app)
    FlaskDynaconf(app)

    mail.init_app(app)

    app.register_blueprint(default_view)
    app.register_blueprint(api_marker, url_prefix="/api/marker/")

    CORS(default_view, max_age=30 * 86400)
    CORS(api_marker, max_age=30 * 86400)

    jwt.init_app(app)
    db.init_app(app)
    Migrate(app, db, render_as_batch=True)

    return app