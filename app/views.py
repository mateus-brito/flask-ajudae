from flask import abort, Blueprint, render_template, Response, redirect, url_for, request, jsonify, current_app
import re
import os
import json
import sys
import hashlib
from sqlalchemy import func
from app.models import *
from os import environ 
import datetime
import requests
import time 
from werkzeug.utils import secure_filename
from PIL import Image
import hashlib
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from app.mail import mail
from flask_mail import Message

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

getHash512 = lambda text : hashlib.sha512(str(text).encode("UTF-8")).hexdigest()
check_email = lambda email : re.search('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$',email)
serialize = lambda query: jsonify([ {**row._asdict() } for row in query ])
serialize_custom = lambda query, custom: jsonify([ {**row._asdict(), **custom(row)} for row in query ])

default_view = Blueprint('default_view', __name__,template_folder='templates')

@default_view.route('/', methods=['GET', 'POST'])
@default_view.route('/sobre', methods=['GET', 'POST'])
@default_view.route('/contato', methods=['GET', 'POST'])
@default_view.route('/como-ajudar', methods=['GET', 'POST'])
def index():
    return render_template("index.html")

@default_view.route("/contact", methods=['POST'])
def contact():
    token = request.json.get('token', '')
    name = request.json.get('name')
    email = request.json.get('email')
    message = request.json.get('message')

    if len(token) > 0:
        token = "token: {token}\n".format(token=token)

    final_message = """
    {token}
    name: {name}
    email: {email}

    message: {message}
    """.format(token=token, name=name, email=email, message=message)

    subject = "DENÚNCIA" if len(token) > 0 else "Contato"

    with mail.connect() as conn:
        msg = Message(sender=current_app.config['MAIL_USERNAME'],
                    recipients=[ current_app.config['MAIL_USERNAME'] ],
                    body=final_message,
                    subject=subject)
        conn.send(msg)
    return {},200

@default_view.route("/logout/", methods=['GET', 'POST'])
def logout():
    return redirect(url_for('default_view.index'))

@default_view.route('/register', methods=['POST'])
def register():
    if not request.is_json:
        return jsonify({"error": "Missing JSON in request"}), 400

    email = request.json.get('email', None)
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not email:
        return jsonify({"error": "Missing email parameter"}), 400
    elif not check_email(email):
        return jsonify({"error": "Email invalid"}), 400
    if not username:
        return jsonify({"error": "Missing username parameter"}), 400
    if not password:
        return jsonify({"error": "Missing password parameter"}), 400

    user_exists = bool(User.query.filter_by(email=email).first())

    if user_exists:
        return jsonify({"error": "Email já cadastrado."}), 400

    try:
        user = User(email=email, username=username, password=getHash512(password))
        db.session.add(user)
    except Exception: 
        return jsonify({"error": "problemas na conexão."}), 400
        db.session.rollback()
    finally:
        db.session.commit()

    expires = datetime.timedelta(minutes=30)
    access_token = create_access_token(identity=user.email, expires_delta=expires)
    ret = {'token': access_token, 'username': user.username, 'id': user.id}
    return jsonify(ret), 200

@default_view.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"error": "Missing JSON in request"}), 400

    email = request.json.get('email', None)
    password = request.json.get('password', None)

    if not email:
        return jsonify({"error": "Missing email parameter"}), 400
    if not password:
        return jsonify({"error": "Missing password parameter"}), 400

    user = User.query.filter_by(email=email,password=getHash512(password)).first()

    if not user:
        return jsonify({"error": "Email ou senha inválido."}), 401

    expires = datetime.timedelta(minutes=120)
    access_token = create_access_token(identity=user.email, expires_delta=expires)
    ret = {'token': access_token, 'username': user.username, 'id': user.id}
    return jsonify(ret), 200

@default_view.route("/address/geocode", methods = ["GET"])
def address_get():
    lat = request.args.get('lat', None)
    lng = request.args.get('lng', None)

    if(lat == None or lng == None):
        abort(404)
    result = extract_lat_long_via_address(lat, lng)
    return jsonify(result)

def extract_lat_long_via_address(lat, lng):
    rua, rua_num, city, state, country = None, None, None, None, None
    api_key = environ.get('FLASK_GOOGLE_CREDENTIALS')
    base_url = "https://maps.googleapis.com/maps/api/geocode/json"
    endpoint = f"{base_url}?latlng={lat},{lng}&key={api_key}"
    # see how our endpoint includes our API key? Yes this is yet another reason to restrict the key
    r = requests.get(endpoint)
    print(endpoint, file=sys.stderr)
    if r.status_code not in range(200, 299):
        return None, None
    try:
        '''
        This try block incase any of our inputs are invalid. This is done instead
        of actually writing out handlers for all kinds of responses.
        '''
        results = r.json()['results']

        rua = getAddress(results, 'route', 'long_name')
        rua_num = getAddress(results, 'street_number', 'long_name')
        city = getAddress(results, 'locality', 'short_name')
        state = getAddress(results, 'administrative_area_level_1', 'short_name')
        country = getAddress(results, 'country', 'short_name')
    except:
        print("Erro")
    return {
        'rua': rua, 
        'rua_num': rua_num,
        'city': city,
        'state': state,
        'country': country
    }

def getAddress(results, target, taget_param):
    for result in results:
        address_components = result['address_components']
        for ac in address_components:
            address_types = ac['types']
            if target in address_types:
                return ac[taget_param]
    return None

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def save_optmize_image(file, prefix="", sufix="",path_prefix=""):
    basedir = os.path.abspath(os.path.dirname(__file__))
    filename = prefix + secure_filename(str(file.filename))+ sufix
    pathfile = os.path.join(basedir,"static/fotos/"+path_prefix, filename)
    file.save(pathfile)
    return optmize_image(pathfile)

def optmize_image(pathfile):
    file = Image.open(pathfile)
    name_image = file.filename.split(".")[0]
    type_image = file.filename.split(".")[1]
    rgb_file = file.convert("RGB")
    rgb_file.save(name_image+".jpg", format="JPEG", quality=70)
    if type_image.lower() != "jpg":
        os.remove(pathfile)
    return str(str(name_image).split("/")[-1:][0])+".jpg"