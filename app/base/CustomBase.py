from app.database import db
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base

class CustomBase(db.Model):
    __abstract__ = True

    #__table_args__ = {'mysql_engine': 'InnoDB'}

    created_on = db.Column(db.DateTime, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)